//
// Created by alexb on 11.03.2018.
//

#include "LoopList.h"

LoopList::LoopList() {
    head = new TMonom();
    head->setNext(head);
}

LoopList &LoopList::operator=(const LoopList & src) {
    this->~LoopList();

    head = new TMonom();

    TMonom *cur = src.head->getNext();
    TMonom *last = head;

    while (cur != src.head) {
        last->setNext(new TMonom(*cur));
        last = last->getNext();
        last->setNext(head);
        cur = cur->getNext();
    }
	return *this;
}

void LoopList::add(int coeff, int convolution) {
    TMonom *m = new TMonom(coeff, convolution);
    TMonom *prev = head;
    TMonom *cur = head->getNext();
    while (convolution < cur->getConvolution()) {
        prev = cur;
        cur = cur->getNext();
    }

    if (cur->getConvolution() == convolution) {
        int nCoeff = cur->getCoeff() + coeff;
        if (nCoeff != 0) {
            cur->setCoeff(nCoeff);
        } else if (cur != head) {
            prev->setNext(cur->getNext());
            cur->setNext(nullptr);
            delete cur;
        }
    } else {
        prev->setNext(m);
        m->setNext(cur);
    }
}

LoopList::LoopList(const LoopList &src) {
    head = new TMonom();

    TMonom *cur = src.head->getNext();
    TMonom *last = head;

    while (cur != src.head) {
        last->setNext(new TMonom(*cur));
        last = last->getNext();
        last->setNext(head);
        cur = cur->getNext();
    }
}

std::string LoopList::toString(int maxDeg, int n) {
    std::string res = "";
    TMonom *cur = head->getNext();

    while (cur != head) {
        std::string st = cur->toString(maxDeg, n);
        if (!st.empty() && st[0] != '-') {
            st = "+" + st;
        }
        res += st;
        cur = cur->getNext();
    }
    if (res.empty()) {
        res = "0";
    }

    return res;
}

LoopList::~LoopList() {
    TMonom *cur = head->getNext();
    while (cur != head) {
        head->setNext(cur->getNext());
        cur->setNext(nullptr);
        delete cur;
        cur = head->getNext();
    }
    head->setNext(nullptr);
    delete head;
}

LoopList LoopList::operator+(const LoopList &rhs) {
    LoopList res(*this);
    TMonom *cur = rhs.head->getNext();

    while (cur != rhs.head) {
        res.add(cur->getCoeff(), cur->getConvolution());
        cur = cur->getNext();
    }

    return res;
}

LoopList LoopList::operator*(const int &rhs) {
    LoopList res(*this);

    TMonom *cur = res.head->getNext();

    while (cur != res.head) {
        cur->setCoeff(cur->getCoeff() * rhs);
        cur = cur->getNext();
    }

    return res;
}

LoopList LoopList::operator-(const LoopList &rhs) {
    LoopList res(*this);

    res = res + ((LoopList)rhs * (-1));

    return res;
}

LoopList LoopList::multiply(LoopList rhs, int maxDeg, int varCount) {
    LoopList res;

    TMonom *cur = head->getNext();

    while (cur != head) {
        TMonom *m = rhs.head->getNext();
        while (m != rhs.head) {
            TMonom r = cur->multiply(*m, maxDeg, varCount);
            res.add(r.getCoeff(), r.getConvolution());
            m = m->getNext();
        }
        cur = cur->getNext();
    }

    return res;
}


