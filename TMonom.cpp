//
// Created by alexb on 11.03.2018.
//

#include "TMonom.h"
#include <cstdlib>

TMonom::TMonom(const TMonom & src) {
    convolution = src.convolution;
    coeff = src.coeff;
    next = this;
}

TMonom::~TMonom() {
    next = nullptr;
}

TMonom &TMonom::operator=(const TMonom & src) {
    coeff = src.coeff;
    convolution = src.convolution;
    next = this;

    return *this;
}

int TMonom::getCoeff() {
    return coeff;
}

void TMonom::setCoeff(int coeff) {
    this->coeff = coeff;
}

int TMonom::getConvolution() {
    return convolution;
}

void TMonom::setConvolution(int convolution) {
    this->convolution = convolution;
}

TMonom *TMonom::getNext() {
    return next;
}

void TMonom::setNext(TMonom * ptr) {
    next = ptr;
}

std::string TMonom::toString(int maxDeg, int varCount) {
    std::string res = "";
    int remainder, quotient = convolution;
    for (int i = varCount; i > 0; i--) {
        remainder = quotient % maxDeg;
        quotient /= maxDeg;
        if (remainder == 1) {
            res = "x" + std::to_string(i) + res;
        }
        if (remainder > 1) {
            res = "x" + std::to_string(i) + "^" + std::to_string(remainder) + res;
        }
    }

    if (coeff != 1 && coeff != -1) {
        res = std::to_string(coeff) + res;
    } else if (coeff == -1) {
        res = "-" + res;
    }

    return res;
}

TMonom TMonom::multiply(const TMonom &rhs, int maxDeg, int varCount) {
    TMonom res(*this);

    res.setCoeff(coeff * rhs.coeff);

    int *degs1 = new int[varCount];
    int *degs2 = new int[varCount];

    int c1 = convolution, c2 = rhs.convolution;

    for (int i = varCount; i > 0; i--) {
        degs1[i-1] = c1 % maxDeg;
        degs2[i-1] = c2 % maxDeg;

        c1 /= maxDeg;
        c2 /= maxDeg;

        degs1[i-1] += degs2[i-1];

        if (degs1[i-1] >= maxDeg) {
            return TMonom();
        }
    }

    int s = 0;

    for (int i = 0; i < varCount; i++) {
        s = s * maxDeg + degs1[i];
    }

    res.convolution = s;

    delete[] degs1;
    delete[] degs2;

    return res;
}

int TMonom::stringToCoefficient(std::string str, int maxDeg, int varCount) {
    if (str.empty()) {
        return 0;
    }

    int start = 0;

    if (str[0] == '-') {
        start = 1;
    }

    if (str[start] == 'x') {
        return start == 0 ? 1 : -1;
    }

    if (str[start] >= '0' && str[start] <= '9') {
        std::string num;

        for (int i = start; i < str.length() && str[i] >= '0' && str[i] <= '9'; i++) {
            num += str[i];
        }

        int res = std::stoi(num);

        if (start == 1) {
            res *= -1;
        }

        return res;
    }

    return 0;
}

int TMonom::stringToConvolution(std::string str, int maxDeg, int varCount) {
    int *ni = new int[varCount];

    for (int i = varCount; i > 0; i--) {
        std::string t = "x" + std::to_string(i);
        unsigned long pos = str.find(t);
        if (pos == std::string::npos) {
            ni[i-1] = 0;
        } else {
            str.erase(pos, t.length());

            if ((pos < str.length() && str[pos] == 'x') || pos >= str.length()) {
                ni[i-1] = 1;
            } else if (pos < str.length() && str[pos] == '^') {
                str.erase(pos, 1);
                std::string num = "";

                while (pos < str.length()
                       && (str[pos] >= '0' && str[pos] <= '9')) {
                    num += str[pos];
                    str.erase(pos, 1);
                }

                ni[i-1] = std::stoi(num);

                if (ni[i - 1] > maxDeg) {
                    return -1;
                }
            }
        }
    }

    int s = 0;

    for (int i = 0; i < varCount; i++) {
        s = s * maxDeg + ni[i];
    }

    return s;
}


