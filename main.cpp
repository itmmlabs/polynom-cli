#include <iostream>
#include <string>
#include "TMonom.h"
#include "LoopList.h"

int main() {

    std::cout << "Max degree: ";
    int maxDeg;

    std::cin >> maxDeg;

    std::cout << "Var count: ";

    int varCount;

    std::cin >> varCount;

    std::cout << "Monom: ";

    std::string res;
    std::cin.ignore();
    std::getline(std::cin, res);

//    std::cout << res << std::endl;

    std::cout << TMonom::stringToCoefficient(res, maxDeg, varCount) << std::endl;
    std::cout << "Conv: " << TMonom::stringToConvolution(res, maxDeg, varCount) << std::endl;

    TMonom monom1(
            TMonom::stringToCoefficient(res, maxDeg, varCount),
            TMonom::stringToConvolution(res, maxDeg, varCount)
    );

    std::cout << "Str " << monom1.toString(maxDeg, varCount) << std::endl;

//    std::cin.ignore();
    std::cout << "Monom 2: ";
    std::getline(std::cin, res);

    TMonom monom2(
            TMonom::stringToCoefficient(res, maxDeg, varCount),
            TMonom::stringToConvolution(res, maxDeg, varCount)
    );

    std::cout << "Coeff " << monom2.getCoeff() << std::endl;
    std::cout << "Conv " << monom2.getConvolution() << std::endl;
    std::cout << "Str " << monom2.toString(maxDeg, varCount) << std::endl;

    TMonom mul = monom1.multiply(monom2, maxDeg, varCount);

    std::cout << "monom1 * monom2 " << mul.toString(maxDeg, varCount) << std::endl;

    LoopList *list = new LoopList();

    list->add(monom1.getCoeff(), monom1.getConvolution());
    list->add(monom2.getCoeff(), monom2.getConvolution());

    std::cout << "List is " << list->toString(maxDeg, varCount) << std::endl;

    LoopList *list2 = new LoopList();

    std::cout << "Monom 3: ";

    std::getline(std::cin, res);

    std::cout << res << std::endl;

    monom1 = TMonom(
            TMonom::stringToCoefficient(res, maxDeg, varCount),
            TMonom::stringToConvolution(res, maxDeg, varCount)
    );

    std::cout << "Coeff " << monom1.getCoeff() << std::endl;
    std::cout << "Conv " << monom1.getConvolution() << std::endl;
    std::cout << "Str " << monom1.toString(maxDeg, varCount) << std::endl;

    std::cout << "Monom 4: ";
    std::getline(std::cin, res);

    monom2 = TMonom(
            TMonom::stringToCoefficient(res, maxDeg, varCount),
            TMonom::stringToConvolution(res, maxDeg, varCount)
    );

    std::cout << "Coeff " << monom2.getCoeff() << std::endl;
    std::cout << "Conv " << monom2.getConvolution() << std::endl;
    std::cout << "Str " << monom2.toString(maxDeg, varCount) << std::endl;

    list2->add(monom1.getCoeff(), monom1.getConvolution());
    list2->add(monom2.getCoeff(), monom2.getConvolution());

    std::cout << "List 2 is " << list2->toString(maxDeg, varCount) << std::endl;

    LoopList resList = (*list) + (*list2);

    std::cout << resList.toString(maxDeg, varCount) << std::endl;

    resList = (*list) - (*list2);
    std::cout << resList.toString(maxDeg, varCount) << std::endl;

    resList = list->multiply(*list2, maxDeg, varCount);
    std::cout << resList.toString(maxDeg, varCount) << std::endl;


    delete list;
    delete list2;

    return 0;

}