//
// Created by alexb on 11.03.2018.
//

#ifndef POLYNOMS_TMONOM_H
#define POLYNOMS_TMONOM_H

#include <string>

class TMonom {
private:
    int coeff;
    int convolution;
    TMonom *next;
public:
    explicit TMonom(int coeff = 0, int convolution = -1) : coeff(coeff), convolution(convolution), next(this) {};

    TMonom(const TMonom &);

    ~TMonom();

    TMonom &operator=(const TMonom &);

    int getCoeff();

    void setCoeff(int);

    int getConvolution();

    void setConvolution(int);

    TMonom* getNext();

    void setNext(TMonom *);

    std::string toString(int maxDeg, int varCount);

    static int stringToCoefficient(std::string str, int maxDeg, int varCount);

    static int stringToConvolution(std::string str, int maxDeg, int varCount);

    TMonom multiply(const TMonom &rhs, int maxDeg, int varCount);
};


#endif //POLYNOMS_TMONOM_H
