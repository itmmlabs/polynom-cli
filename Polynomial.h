//
// Created by Александр Баташев on 18.03.2018.
//

#ifndef POLYNOMS_POLYNOM_H
#define POLYNOMS_POLYNOM_H


#include "LoopList.h"

class Polynomial {
private:
    int maxDeg;
    int varCount;
    LoopList *list;
    std::string polynomialStr;

    void stringToWords(std::string str, std::string words[], int &size);

    int stringToCoeff(std::string str);

    int stringToConvolution(std::string str);

public:
    explicit Polynomial(std::string polynomialStr = "", int maxDeg = 10, int varCount = 3);

    Polynomial(const Polynomial &);

    Polynomial &operator=(const Polynomial &);

    ~Polynomial();

    void addMonom(std::string monomStr);

    Polynomial operator+(const Polynomial &);

    Polynomial operator-(const Polynomial &);

    Polynomial operator*(const Polynomial &);

    std::string toString();
};


#endif //POLYNOMS_POLYNOM_H
