//
// Created by alexb on 11.03.2018.
//

#ifndef POLYNOMS_LOOPLIST_H
#define POLYNOMS_LOOPLIST_H


#include "TMonom.h"

class LoopList {
private:
    TMonom *head;
public:
    LoopList();

    LoopList(const LoopList &);

    ~LoopList();

    LoopList &operator=(const LoopList &);

    void add(int coeff, int convolution);

    LoopList operator+(const LoopList &rhs);

    LoopList operator-(const LoopList &rhs);

    LoopList operator*(const int &rhs);

    std::string toString(int maxDeg, int n);

    LoopList multiply(LoopList rhs, int maxDeg, int varCount);

};


#endif //POLYNOMS_LOOPLIST_H
